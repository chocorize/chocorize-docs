+++
title = "Idea and Approach"
description = "Chocorize is a concept and web service solving the legal challenges of accepting minors as users on internet service."
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Chocorize is a concept and web service solving the legal challenges of accepting minors as users on internet service."
toc = true
top = false
+++

## Introduction

With most websites and online services requiring the submission of some
personal information, these sites are required to ask for consent for
their data processing activities. This requirement is imposed by differing
laws, e.g. the [GDPR](https://ec.europa.eu/info/law/law-topic/data-protection_en)
(General Data Protection Regulation) in the European Union.

Generally, adults can freely decide whether they want to consent to any
legal terms, but minors cannot always freely do so.

Taking the GDPR as an example, it defines a set of quite complex rules
concerning what and when minors can consent to, and in most cases, these
rules boil down to minor's under the age of 16 years require additional
consent of a legal guardian.

In addition to privacy laws, other fields of endeavour also might require
consent of a legal guardian, e.g. agreement with a software license or a
CLA (Contributor License Agreement) in free software/open source development.

### Challenge

Verifying and tracking the identity, age, and required consents for a person
implies several challenges for service operators:

 * Verifying the identity and age can consume a lot of resources
 * Verifying the identity requires collection of sensitive personal information
   * Operators might not even want to collect legal names, dates of birth, etc.,
     so verifying identies itself raises privacy concerns

As a result, many operators decide to ban users under 16 years altogether,
so they do not need to carry the burden of verifying users' ages.

### Approach

To take the burden of identity and age verification from the single operator,
chocorize delegates the necessary steps to a third party, which handles the
legal requirements in accordance with their policies. It defines open APIs
that allow operators to hand off verification to an external service, which
can be offered by arbitrary providers.

[chocorize.me](https://chocorize.me) offers a centralised instance which can
be used by children, parents, and so-called "sponsoring delegates", and that
builds a network of people needing consent (minors), people giving consent
(legal guardians), and organisations observing the verification itself.

In addition, chocorize can hand off verification to other entities that happen
to have information to do the verification on file, e.g. a school's student
information system.
