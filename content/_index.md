+++
title = "Chocorize"


# The homepage contents
[extra]
lead = '<b>Chocorize</b> is an authorization platform that helps website operators to safely and legally welcome young users, children to understand privacy terms, and parents to protect them.'
url = "/docs/intro/"
url_button = "Learn about the idea →"
repo_version = "Code on Codeberg"
repo_license = "EUPL-1.2"
repo_url = "https://codeberg.org/Chocorize"

# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "/docs/intro/"
weight = 10

#[[extra.menu.main]]
#name = "Blog"
#section = "blog"
#url = "/blog/"
#weight = 20

[[extra.list]]
title = "For website operators"
content = "Offload verification and tracking of consents and relevant personal attributes, like age and existence of parental consent, to the Chocorize service."

[[extra.list]]
title = "For children"
content = "Learn to understand terms and conditions and to use websites and apps autonomously, with transparent consent by legal guardians."

[[extra.list]]
title = "For parents"
content = "Help children to gain access to the websites and services they want to use, while keeping track of all terms and policies."

[[extra.list]]
title = "Privacy-aware by default"
content = "Personal attributes, verification media, and identification data are only stored as long as necessary and forwarded encrypted. Website operators only receive an attestation of the requested verification."

[[extra.list]]
title = "Standards-compliant API"
content = "Fully REST-compliant API for making and tracking requests, and authorisation leveraging OpenID Connect for maximum compatibility."

[[extra.list]]
title = "Decentralised, but centralisable"
content = "Real-world verification is offloaded to to a network of sponsoring organisations, and Chocorize services can federate to decentralise verification and proofs of identification."

+++
